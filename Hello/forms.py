from django import forms
from . import models

class StatForm(forms.ModelForm):
    status=forms.CharField(widget=forms.Textarea(attrs={
        "style":"width:1000px; margin:20px; resize:none",
        "class":"form-control",
        "required":True,
        "placeholder":"Your Status Here",
    }), label="")
    class Meta:
        model=models.Stat
        fields=['status']